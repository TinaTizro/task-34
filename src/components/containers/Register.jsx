import React from 'react';
import RegisterForm from '../forms/RegisterForm';


const Register = () => {
    const handleRegisterClicked = (result) => {
        console.log('Triggered from RegisterForm', result);
        if (result) {
            //redirect.
        }
        
    };
    return(
    <div>
        <h1> Register for survey puppy</h1>
        <RegisterForm  click={ handleRegisterClicked} />
    </div>
    )
};


 
export default Register ;